from django.contrib import admin
from . import models

@admin.register(models.PasswordsModel)
class PasswordAdmin(admin.ModelAdmin):

    readonly_fields = ['password']
    list_filter = ['type', 'username']
    list_display = ['user', 'type', 'host','port', 'url', 'name', 'username', 'password']

