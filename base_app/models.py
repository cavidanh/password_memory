from django.db import models
from django.contrib.auth import get_user_model
from django.utils.translation import ugettext_lazy as _
User = get_user_model()


class PasswordsModel (models.Model):
    passwords_type = (
        ('FTP', 'FTP'),
        ('SSH', 'SSH'),
        ('ADMIN', 'ADMIN'),
        ('EMAIL', 'EMAIL')
    )
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    type =  models.CharField(max_length=7, choices=passwords_type)
    username = models.CharField(max_length=125)
    host = models.CharField(max_length=125, null=True, blank=True)
    port = models.IntegerField( null=True, blank=True)
    password = models.CharField(_('password'), max_length=128)
    url = models.CharField(max_length=125, null=True, blank=True)
    name = models.CharField(max_length=125, null=True, blank=True)
    create_date = models.DateTimeField(auto_now_add=True, null=True, blank=True)





