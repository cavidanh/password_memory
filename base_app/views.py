from django.http import HttpResponse, JsonResponse
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import ListView
from django.contrib.auth.models import User, auth
from django.contrib import messages
from . import models
from base_app import forms
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from .tasks import send_update_email_task, send_create_email_task, send_delete_email_task
import logging

# Get an instance of a logger
logger = logging.getLogger('APPNAME')


# Userlerin logini bu viewda aparilir
def login(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']

        user = auth.authenticate(username=username, password=password)

        if user is not None:
            auth.login(request, user)
            return redirect('home')

        else:
            messages.info(request, 'Invalid credentials')
            return redirect('login')

    else:
        return render(request, 'login.html')


# Userlerin logoutlari bu viewda aparilir
def logout(request):
    auth.logout(request)
    return redirect('login')


# Userlerin qeydiyyati bu viewda aparilir
def register(request):
    if request.method == 'POST':
        first_name = request.POST['first_name']
        last_name = request.POST['last_name']
        user_name = request.POST['username']
        email = request.POST['email']
        password1 = request.POST['password1']
        password2 = request.POST['password2']

        if password1 == password2:
            if models.User.objects.filter(username=user_name).exists():
                messages.info(request, 'Username Taken')
                return redirect('register')
            elif models.User.objects.filter(email=email).exists():
                messages.info(request, 'Email Taken')
                return redirect('register')
            else:
                user = models.User.objects.create_user(username=user_name,
                                                       password=password1,
                                                       email=email,
                                                       first_name=first_name,
                                                       last_name=last_name)
                user.save()
                print('user created')

        else:
            messages.info(request, "password not matching...")
            return redirect('register')
        return redirect('login')

    else:
        return render(request, 'register.html')


# Esas sehifenin viewsi class base ile yazilmisdir.
class HomeView(LoginRequiredMixin, ListView):
    model = models.PasswordsModel
    template_name = "index.html"
    paginate_by = 3

    def get_queryset(self):
        new_context = models.PasswordsModel.objects.filter(
            user=self.request.user
        ).order_by('-id')
        return new_context

    def get_context_data(self, **kwargs):
        context = super(HomeView, self).get_context_data(**kwargs)
        data = self.get_queryset()
        pagination = Paginator(data, self.paginate_by)
        context["lists"] = pagination.get_page(self.request.GET.get('page', 1))
        context["page_range"] = pagination.page_range
        return context

    def post(self, request):
        if request.is_ajax():
            if request.POST.get('check') == 'edit':
                id = request.POST.get('id')
                data = models.PasswordsModel.objects.filter(id=id).first()
                form = forms.PasswordFTPForm(request.POST, instance=data)
                if form.is_valid():
                    password = form.save(commit=False)
                    password.user = request.user
                    password.save()
                    send_update_email_task.delay(email=password.user.email,
                                                 msg=f'Sizin {password.type} passwordunuz deyisdirildi')
                    logger.info(
                        f'{password.create_date}: Bu {password.type} password type i {request.user} terefinden deyisdirildi')
                    return JsonResponse({
                        'url': '/dashboard/'
                    })

            else:
                form = forms.PasswordFTPForm(request.POST)
                if form.is_valid():
                    password = form.save(commit=False)
                    password.user = request.user
                    password.save()
                    send_create_email_task.delay(email=password.user.email,
                                                 msg=f'Sizin {password.type} passwordunuz yaradildi')
                    logger.info(
                        f'{password.create_date}: Bu {password.type} password type i {request.user} terefinden yaradildi')
                    return JsonResponse({
                        'url': '/dashboard/'
                    })


# Password Typelarin filter hissesi bu viewda aparilir.
def filter_view(request, slug):
    data = models.PasswordsModel.objects.filter(type=slug, user=request.user).order_by('-id')
    pagination = Paginator(data, 3)
    context = {}
    context["lists"] = pagination.get_page(request.GET.get('page', 1))
    context["page_range"] = pagination.page_range

    if request.method == 'POST':
        if request.is_ajax():
            if request.POST.get('check') == 'edit':
                id = request.POST.get('id')
                data = models.PasswordsModel.objects.filter(id=id).first()
                form = forms.PasswordFTPForm(request.POST, instance=data)
                if form.is_valid():
                    password = form.save(commit=False)
                    password.user = request.user
                    password.save()
                    send_update_email_task.delay(email=password.user.email,
                                                 msg=f'Sizin {password.type} passwordunuz deyisdirildi')
                    logger.info(
                        f'{password.create_date}: Bu {password.type} password type i {request.user} terefinden deyisdirildi')
                    return JsonResponse({
                        'url': '/dashboard/'
                    })

            else:
                form = forms.PasswordFTPForm(request.POST)
                if form.is_valid():
                    password = form.save(commit=False)
                    password.user = request.user
                    password.save()
                    send_create_email_task.delay(email=password.user.email,
                                                 msg=f'Sizin {password.type} passwordunuz yaradildi')
                    logger.info(
                        f'{password.create_date}: Bu {password.type} password type i {request.user} terefinden yaradildi')
                    return JsonResponse({
                        'url': '/dashboard/'
                    })
            # else:
            #     context['form'] = form

    return render(request, 'index.html', context)


# home view da create ucun buttonlardan birine basanda bu view a request gelir form goturur
@login_required()
def form_view(request, slug):
    form = forms.PasswordFTPForm(initial={'type': slug})
    if slug == 'ADMINN':
        form = forms.PasswordFTPForm(initial={'type': 'ADMIN'})

    context = {
        'form': form,
        'slug': slug
    }
    return render(request, 'forms.html', context)


# edit ve ya show detail olunanda bu view a request gelir
@login_required()
def detail_view(request, id):
    data = models.PasswordsModel.objects.filter(id=id).first()

    context = {
        'type': data.type,
        'form': forms.PasswordFTPForm(instance=data),
        'id': id
    }
    return render(request, 'details.html', context)


# delete bu view a request gedir
@login_required()
def delete_view(request, id):
    context = {
        'id': id
    }
    if request.method == 'DELETE':
        data = models.PasswordsModel.objects.filter(id=id).first()

        data.delete()
        send_delete_email_task.delay(email=data.user.email, msg=f'Sizin {data.type} passwordunuz silindi')
        logger.info(f'{data.create_date}: Bu {data.type} password type i {request.user} terefinden silindi')
        return JsonResponse({
            'url': '/dashboard/'
        })
    return render(request, 'delete.html', context)
