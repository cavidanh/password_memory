from celery import shared_task
from time import sleep
from django.core.mail import send_mail, EmailMultiAlternatives
from django.conf import settings


@shared_task
def send_create_email_task(email,msg):
    send_mail(subject="Password Create",message=msg,from_email=settings.EMAIL_HOST_USER,recipient_list=[email])

    return 'success'


@shared_task
def send_update_email_task(email,msg):
    send_mail(subject="Password Change",message=msg,from_email=settings.EMAIL_HOST_USER,recipient_list=[email])

    return 'success'



@shared_task
def send_delete_email_task(email,msg):
    send_mail(subject="Password Delete",message=msg,from_email=settings.EMAIL_HOST_USER,recipient_list=[email])

    return 'success'
