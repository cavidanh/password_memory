from django import forms
from . import models


class PasswordFTPForm(forms.ModelForm):
    class Meta:
        model = models.PasswordsModel
        fields = [
           'type', 'host', 'port','url', 'name', 'username', 'password'
        ]

